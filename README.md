<!-- writeme -->
Drutopia Findit Program
=======================

An ongoing program available from a service providing organization, distinct from one-time event or an always-available place.

 * https://gitlab.com/find-it-program-locator/drutopia_findit_program
 * Issues: https://gitlab.com/find-it-program-locator/drutopia_findit_program/issues
 * Source code: https://gitlab.com/find-it-program-locator/drutopia_findit_program/tree/8.x-1.x
 * Keywords: find it, drupal, drutopia, program
 * Package name: drupal/drutopia_findit_program


### Requirements

 * drupal/config_actions ^1.0-beta1
 * drupal/drutopia_core ^1.0-alpha1
 * drupal/drutopia_findit_organization 1.x-dev
 * drupal/entity_reference_validators ^1.0@alpha
 * drupal/serviceprovider 1.x-dev


### License

GPL-2.0+

<!-- endwriteme -->
