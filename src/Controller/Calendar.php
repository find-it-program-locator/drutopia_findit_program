<?php

namespace Drupal\drutopia_findit_program\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\drutopia_findit_program\Filters;

class Calendar extends ControllerBase {

  /**
   * @var EntityTypeManagerInterface;
   */
  var $entityTypeManager;

  /**
   * @var \Drupal\drutopia_findit_program\Filters;
   */
  protected $filterService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('drutopia_findit_program.taxonomy_filters')
    );
  }
  public function __construct(EntityTypeManagerInterface $entityTypeManager, Filters $filters) {
    $this->entityTypeManager = $entityTypeManager;
    $this->filterService = $filters;
  }

  /**
   *
   */
  public function calendar() {
    $default_values = [
      'from' => 'now',
      'to' => '+2 months',
      'limit' => 100,
      'id' => NULL,
      'organization' => NULL,
      'service' => NULL,
      'activity' => NULL,
      'age' => NULL,
      'grade' => NULL,
    ];
    $taxonomy_filters = $this->filterService->getTaxonomyFieldFilters();

    // Queries.
    $values = [
      'from' => \Drupal::request()->query->get('from'),
      'to' => \Drupal::request()->query->get('to'),
      'limit' => \Drupal::request()->query->get('limit'),
      'id' => \Drupal::request()->query->get('id'),
    ];
    // Getting the filters values.
    foreach ($taxonomy_filters as $filter => $field_name) {
      $values[$filter] = explode(',', \Drupal::request()->query->get($filter));
    }
    $values = array_merge($default_values, array_filter($values));
    $query = $this->entityTypeManager()->getStorage('node')->getQuery('AND');
    $query->condition('type', 'findit_event');
    $query->condition('status', 1);
    if (!is_null($values['id'])) {
      list($nid, $timestamp) = explode("-", $values['id']);
      // $date = date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $timestamp);
      $query->condition('nid', $nid);
      $query->condition('field_findit_opportunity_dates.value', $timestamp, '=');
      // Override the date  with the value of the ID.
      $values['from'] = $timestamp; // date;
      unset($values['to']);
    }
    else {
      if (!is_numeric($values['limit']) || $values['limit'] < 1 || $values['limit'] > 100) {
        return new JsonResponse(['error' => 'Invalid LIMIT value'], 400);
      }
      $from = new DrupalDateTime($values['from']);
      $from->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
      $from_timestamp = $from->getTimestamp(); // format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
      $query->condition('field_findit_opportunity_dates.value', $from_timestamp, '>=');

      $to = new DrupalDateTime($values['to']);
      $to->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
      $to_timestamp = $to->getTimestamp(); // format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT);
      $query->condition('field_findit_opportunity_dates.value', $to_timestamp, '<=');

      $query->range(0, $values['limit']);
    }

    // Adding the taxonomy filters in the query.
    foreach ($taxonomy_filters as $filter => $field_name) {
      if ($values[$filter] && ctype_digit(implode('', $values[$filter]))) {
        $query->condition($field_name, $values[$filter], 'IN');
      }
    }

    $result = $query->execute();
    $output = [];

    // Node ids for events imported from the library.
    $library_created_nids = \Drupal::state()->get('findit_library_sync.created_nids', []);

    foreach ($result as $nid) {
      $entity = $this->entityTypeManager->getStorage('node')->load($nid);
      $url = $entity->toUrl('canonical', ['absolute' => TRUE]);
      $summary = $entity->get('field_findit_short_summary')->first();
      $location = $entity->get('field_findit_locations')->getValue();
      $content = $entity->get('body')->value ?? '';
      $source = '';
      if (in_array($nid, $library_created_nids, FALSE)) {
        $source = 'CITY_LIBRARY';
      }
      $contact = $entity->get('field_findit_contacts')->getValue();
      $contact_details = $this->getContactDetails($contact);
      $dates = $entity->getDates($values['from'], $values['to']);
      foreach ($dates as $date) {
        // The ID will be the nid + the time of the event.
        $id = $entity->id() . "-" . strtotime($date['start_time']);
        $item = [
          '_url' =>  $url->toString(),
          'id' => $id,
          'title' => $entity->getTitle(),
          'description' => (!is_null($summary)) ? $summary->value : '',
          'keywords' => '',
          'start_time' => $date['start_time'],
          'end_time' => $date['end_time'],
          'location' => $this->getLocationName($location),
          'neighborhoods' => [],
          'contact_name' => $contact_details['name'],
          'contact_phone' => $contact_details['phone'],
          'contact_email' => $contact_details['mail'],
          'registration_link' => $this->getRegistrationLink($entity->get('field_findit_registration_url')->getValue()),
          'content' => $content,
          'source' => $source,
          'image'=> $this->getImageUrl($entity->get('field_image')->getValue()),
        ];
        // Adding the filters values into the results.
        foreach ($taxonomy_filters as $filter => $field_name) {
          $item[$filter] = $this->extractReferencedEntitiesValues($entity->get($field_name)->referencedEntities());
        }
        $output[] = $item;
      }
    }
    if (count($output) > $values['limit']) {
      $output = array_slice($output, 0, $values['limit']);
    }
    return new JsonResponse($output);
  }

  /**
   * @param array $url_field
   *
   * @return string
   */
  protected function getRegistrationLink(array $url_field) : string {
    if (empty($url_field)) {
      return '';
    }
    return $url_field[0]['uri'];
  }

  protected function getLocationName(array $location_field) : string {
    if (empty($location_field)) {
      return '';
    }
    $target_id = $location_field[0]['target_id'];
    /** @var \Drupal\node\NodeInterface $location */
    $location = $this->entityTypeManager()->getStorage('node')->load($target_id);
    return $location->getTitle();
  }

  /**
   * @param $image_value
   */
  protected function getImageUrl(array $image_value) : string {
    if (empty($image_value)) {
      return '';
    }
    $target_id = $image_value[0]['target_id'];
    /** @var \Drupal\file\FileInterface $file */
    $file = $this->entityTypeManager()->getStorage('file')->load($target_id);
    return $file->createFileUrl(FALSE);
  }

  protected function getContactDetails(array $contact_field) : array {
    if (empty($contact_field)) {
      return ['name' => '', 'phone' => '', 'email' => ''];
    }
    $target_id = $contact_field[0]['target_id'];
    /** @var \Drupal\file\FileInterface $file */
    $contact = $this->entityTypeManager()->getStorage('profile')->load($target_id);
    $fields = [
      'phone' => $contact->get('crm_phone')->value ?? '',
      'mail' => $contact->get('field_public_email')->value ?? '',
    ];
    if ($contact->bundle() == 'crm_org') {
      $fields['name'] = $contact->get('crm_org_name')->value;
    }
    else {
      $fields['name'] = $contact->get('crm_name')->getValue();
      if (!empty($fields['name'][0])) {
        $fields['name'] = $fields['name'][0]['given'] . " " . $fields['name'][0]['family'];
      }
    }

    return $fields;
  }

  /**
   * Receive the array returned by referencedEntities and return an array with
   * the label and id of the entities.
   *
   * @param array $value
   * @return array
   */
  protected function extractReferencedEntitiesValues(array $values) {
    $results = [];
    foreach ($values as $item) {
      $results[] = [
        'id' => $item->id(),
        'name' => $item->label(),
      ];
    }
    return $results;
  }
}
