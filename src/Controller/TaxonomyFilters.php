<?php

namespace Drupal\drutopia_findit_program\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class TaxonomyFilters extends ControllerBase {

  /**
   * @var \Drupal\drutopia_findit_program\Filters;
   */
  protected $filterService;

  /**
   * @var array
   */
  protected $filters;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->filterService = $container->get('drutopia_findit_program.taxonomy_filters');
    $instance->filters = $instance->filterService->getTaxonomyFilters(['organization']);
    return $instance;
  }

  function getFilters() {
    return new JsonResponse(array_keys($this->filters));
  }

  /**
   *
   * Return JSON array with the possible values of the filter.
   */
  function getValuesByFilter() {
    $default_values = [
      'filter' => NULL,
    ];
    $values = [
      'filter' => \Drupal::request()->query->get('filter'),
    ];
    $values = array_merge($default_values, array_filter($values));

    if (empty($values['filter'])) {
      return new JsonResponse(['error' => 'Invalid FILTER value'], 400);
    }

    return new JsonResponse($this->filterService->getValuesByFilter($values['filter']));
  }
}
