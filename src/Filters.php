<?php


namespace Drupal\drutopia_findit_program;

use Drupal\Core\Entity\EntityTypeManagerInterface;

class Filters {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  var $taxonomy_field_filters = [
    'organization' => 'field_findit_organization',
    'service' => 'field_findit_services',
    'activity' => 'field_findit_activities',
    'age' => 'field_findit_ages',
    'grade' => 'field_findit_grades',
    'eligibility' => 'field_findit_eligibility',
    'accessibility' => 'field_findit_accessibility',
  ];
  var $taxonomy_vocabulary_filters = [
    //'organization' => '', // This is not a vocabulary,
    'service' => 'findit_services',
    'activity' => 'findit_activities',
    'age' => 'findit_ages',
    'grade' => 'findit_grades',
    'eligibility' => 'findit_eligibility',
    'accessibility' => 'findit_accessibility',
  ];

  public function getTaxonomyFieldFilters() {
    return $this->taxonomy_field_filters;
  }

  public function getTaxonomyFilters(array $exclude = []) {
    $exclude = array_flip($exclude);
    return array_diff_key($this->taxonomy_field_filters, $exclude);
  }

  public function getVocabularyFilters() {
    return $this->taxonomy_vocabulary_filters;
  }

  /**
   * @return array.
   */
  public function getValuesByFilter($filter) {
    $query = \Drupal::entityQuery('taxonomy_term');
    $vocabularies = $this->getVocabularyFilters();
    if (!isset($vocabularies[$filter])) {
      return [];
    }
    $query->condition('vid', $vocabularies[$filter]);
    $tids = $query->execute();
    $terms = [];
    foreach ($tids as $tid) {
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($tid);
      $terms[$term->id()] = $term->label();
    }
    return $terms;
  }
}
