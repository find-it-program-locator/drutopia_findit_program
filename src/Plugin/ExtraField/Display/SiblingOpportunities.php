<?php

namespace Drupal\drutopia_findit_program\Plugin\ExtraField\Display;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\extra_field\Plugin\ExtraFieldDisplayBase;
use Drupal\node\NodeInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\Item\Item;
use Drupal\search_api\Query\Query;

/**
 * Example Extra field Display.
 *
 * @ExtraFieldDisplay(
 *   id = "sibling_opportunities",
 *   label = @Translation("Programs and events offered by the same organization as the current program or event."),
 *   bundles = {
 *     "node.findit_program",
 *     "node.findit_event",
 *   }
 * )
 */
class SiblingOpportunities extends ExtraFieldDisplayBase {

  const LIMIT = 5;

  /**
   * @var \Drupal\search_api\Query\Query
   */
  private $query;

  /**
   * @var \Drupal\search_api\Query\Query
   */
  private $events_query;

  /**
   * {@inheritdoc}
   */
  public function view(ContentEntityInterface $entity) {
    $elements = [];

    $organization_ids = [];
    $programs_organization_links = [];
    $events_organization_links = [];
    foreach ($entity->field_findit_organization as $entity_reference) {
      $organization_ids[] = $entity_reference->target_id;
      $organization_entity = $entity_reference->entity;
      if ($organization_entity) {
        $programs_organization_links[$organization_entity->id()] = $organization_entity->toLink($organization_entity->label(), 'canonical', ['fragment' => 'current-programs'])->toRenderable();
        $events_organization_links[$organization_entity->id()] = $organization_entity->toLink($organization_entity->label(), 'canonical', ['fragment' => 'upcoming-events'])->toRenderable();
      }
    }
    // Even though the parent organization is a required field, there are
    // migrated opportunities that do not have this and produce fatal errors:
    // org.apache.solr.search.SyntaxError: Cannot parse 'itm_organizations:()
    if (empty($organization_ids)) {
      return $elements;
    }

    $event_other = '';
    $program_other = '';
    if ($entity->bundle() === 'findit_program') {
      $opportunity = t('program');
      $program_other = t('other');
    }
    else {
      $opportunity = t('event');
      $event_other = t('other');
    }


    $index_id = 'main';  // See parameter name 'drutopia_findit_search.index_id' in services.
    $this->query = new Query(Index::load($index_id));
    $this->query->addCondition('status', NodeInterface::PUBLISHED);
    $conditionGroup = $this->query->createConditionGroup('OR', ['nonobsolete']);
    $conditionGroup->addCondition('dates_end', date(DATE_ISO8601), '>');
    $conditionGroup->addCondition('dates_end', NULL);
    $this->query->addConditionGroup($conditionGroup);
    $this->query->addCondition('nid', $entity->id(), '<>');
    $this->query->addCondition('organizations', $organization_ids, 'IN');
    $this->query->range(0, self::LIMIT);

    $intro_text = \Drupal::translation()->formatPlural(count($organization_ids), "More from the organization that brought you this @opportunity.", "More from the organizations that brought you this @opportunity.", ['@opportunity' => $opportunity]);

    $elements['events_title'] = [
      '#markup' => '<h3 class="title is-size-2">' . t('Related events') . '</h3>',
    ];

    // We use the same query, but with a different 'types' conditions:
    $this->events_query = clone $this->query;
    $this->events_query->addCondition('types', 'findit_event', 'IN');
    $this->events_query->execute();
    $result = $this->events_query->getResults();
    if ($result->getResultCount()) {
      $elements['events_intro'] = [
        '#markup' => '<p>' . $intro_text . '</p>',
      ];
      $map = function (Item $item) {
        return $item->getOriginalObject()->getValue();
      };
      $elements['events'] = \Drupal::entityTypeManager()->getViewBuilder('node')->viewMultiple(array_map($map, $result->getResultItems()), 'teaser');
      $elements['events_more'] = [
        '#theme' => 'drutopia_findit_program_organization_list',
        '#type' => 'events',
        '#items' => $events_organization_links
      ];
    }
    else {
      $nomo_text = \Drupal::translation()->formatPlural(count($organization_ids), "No @other events from this organization.", "No @other events from these organizations.", ['@other' => $event_other]);
      $elements['events_more'] = ['#markup' => '<p>' . $nomo_text . '</p>'];
    }

    $elements['programs_title'] = [
      '#markup' => '<h3 class="title is-size-2">' . t('Related programs') . '</h3>',
    ];
    // We can now continue to use the original query for programs.
    $this->query->addCondition('types', 'findit_program', 'IN');
    $this->query->execute();
    $result = $this->query->getResults();
    if ($result->getResultCount()) {
      $elements['programs_intro'] = [
        '#markup' => '<p>' . $intro_text . '</p>',
      ];
      $map = function (Item $item) {
        return $item->getOriginalObject()->getValue();
      };
      $elements['programs'] = \Drupal::entityTypeManager()->getViewBuilder('node')->viewMultiple(array_map($map, $result->getResultItems()), 'teaser');
      $elements['programs_more'] = [
        '#theme' => 'drutopia_findit_program_organization_list',
        '#type' => 'programs',
        '#items' => $programs_organization_links
      ];

    }
    else {
      $nomo_text = \Drupal::translation()->formatPlural(count($organization_ids), "No @other programs from this organization.", "No @other programs from these organizations.", ['@other' => $program_other]);
      $elements['programs_more'] = ['#markup' => '<p>' . $nomo_text . '</p>'];
    }

    // @TODO Link to a set of results filtered to those offered by an organization
    // Note that $result->getResultCount() gives us the total count, not what our range limits the results to, so we could use
    // if ( $result->getResultCount() > self::LIMIT ) to make the optional link.

    return $elements;
  }

}

