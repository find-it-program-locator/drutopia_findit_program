<?php

namespace Drupal\drutopia_findit_program\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\CompositeConstraintBase;

/**
 * Verify that at least one activity or service is selected.
 *
 * @Constraint(
 *   id = "AtLeastOneActivityOrService",
 *   label = @Translation("An activity or service is selected.", context = "Validation"),
 *   type = "entity:node"
 * )
 */

class AtLeastOneActivityOrServiceConstraint extends CompositeConstraintBase {

  /**
   * @var string
   */
  public $message = 'At least one activity or service must be selected.';

  /**
   * {@inheritdoc}
   */
  public function coversFields() {
    return [
      'field_findit_activities',
      'field_findit_services',
    ];
  }

}
