<?php

namespace Drupal\drutopia_findit_program\Plugin\Validation\Constraint;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;


/**
 * Verify that at least one activity or service is selected.
 */
class AtLeastOneActivityOrServiceConstraintValidator extends ConstraintValidator {

  /*
   * @var \Symfony\Component\Validator\Context\ExecutionContextInterface
   */
  protected $context;

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    if (!$entity->isPublished()) {
      // If the opportunity is not going to be published when the save is complete, do not validate anything.
      return NULL;
    }
    if (!$entity->hasField('field_findit_activities') || !$entity->hasField('field_findit_services')) {
      // If the fields do not exist, do not validate anything.
      return NULL;
    }
    $activities = $entity->get('field_findit_activities')->getValue();
    $services = $entity->get('field_findit_services')->getValue();
    if (!$activities && !$services) {
      // If neither field has a value, we do not pass validation.
      $this->context->addViolation($constraint->message);
    }
    return NULL;
  }

}
