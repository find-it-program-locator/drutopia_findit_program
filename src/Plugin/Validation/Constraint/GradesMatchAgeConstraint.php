<?php

namespace Drupal\drutopia_findit_program\Plugin\Validation\Constraint;

use Drupal\Core\Entity\Plugin\Validation\Constraint\CompositeConstraintBase;

/**
 * Verify that grades are within a reasonable approximation of ages.
 *
 * @Constraint(
 *   id = "GradesMatchAge",
 *   label = @Translation("Grades match ages constraint", context = "Validation"),
 *   type = "entity:node"
 * )
 */

class GradesMatchAgeConstraint extends CompositeConstraintBase {

  /**
   * @var string
   */
  public $message = 'The grades selected do not match the ages selected.';

  /**
   * {@inheritdoc}
   */
  public function coversFields() {
    return [
      'field_findit_ages',
      'field_findit_grades',
    ];
  }

}
