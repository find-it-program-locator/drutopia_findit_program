<?php

namespace Drupal\drutopia_findit_program\Plugin\Validation\Constraint;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\ParamConverter\ParamNotConvertedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Validates the grades match age constraint.
 *
 * Grades are optional so this only kicks in if grades are defined.
 *
 * Ensures grades listed are not impossible for ages listed.  For example, if
 * fifth grade is checkmarked, ages should include 9, 10, 11, 12, or 13.
 */
class GradesMatchAgeConstraintValidator extends ConstraintValidator implements ContainerInjectionInterface {

  /*
   * @var \Symfony\Component\Validator\Context\ExecutionContextInterface
   */
  protected $context;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager')->getStorage('node'));
  }

  /**
   * {@inheritdoc}
   */
  public function validate($entity, Constraint $constraint) {
    if (!$entity->hasField('field_findit_ages') || !$entity->hasField('field_findit_grades')) {
      // If the fields do not exist, do not validate anything.
      return NULL;
    }
    $ages = $entity->get('field_findit_ages')->getValue();
    $grades = $entity->get('field_findit_grades')->getValue();
    if (!$grades || !$ages) {
      // If either field has no value, do not validate anything.
      return NULL;
    }
    if (!$this::isPlausible($ages, $grades)) {
      $this->context->addViolation($constraint->message, ['%url' => $url_string]);
    }
    return NULL;
  }

  /**
   * Returns TRUE if all grades (if any) have plausibly matching ages.
   */
  public static function isPlausible($ages, $grades) {
    foreach ($grades as $grade) {
      // And here begins the pseudocode.  The actual data structure is like this:
      // $ages = [
      //  0 => [
      //    'target_id' = "216",
      //  ],
      // ],
      // etc. and same for grades, it's taxonomy term IDs that would need to be
      // translated into values.  Or more realistically, an interface provided
      // somewhere for relating the two by an admin.
      if ($grade == "5th grade") {
        // This would instead need to be a function loading the theoritical admin-defined grade to age matchups.
        $comparisons = FALSE;
        if ($comparisons && !in_array([9, 10, 11, 12], $ages)) {
          // As soon as we have a grade lacking a plausible age, return FALSE.
          return FALSE;
        }
      }
    }
    return TRUE;
  }
}
